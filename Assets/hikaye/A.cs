﻿using UnityEngine;
using System.Collections;

public class A : MonoBehaviour {

	// Use this for initialization


    public bool show;
    GUIStyle smallFont;
    GUIStyle largeFont;

    void Start()
    {

        largeFont = new GUIStyle();


        largeFont.fontSize = 32;
    }
    void OnTriggerStay()
    {
        show = true;
    }
    void OnTriggerExit()
    {
        show = false;
    }

    void OnGUI()
    {

        if (show == true)
        {
            GUI.Label(new Rect(300, 500, 1000, 20), "Yıl 3016. Her yıl dünya nüfusu artıyor.", largeFont);
        }
    }
}
