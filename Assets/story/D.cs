﻿using UnityEngine;
using System.Collections;

public class D : MonoBehaviour {


    public bool show;
    GUIStyle smallFont;
    GUIStyle largeFont;

    void Start()
    {

        largeFont = new GUIStyle();


        largeFont.fontSize = 32;
    }
    void OnTriggerStay()
    {
        show = true;
    }
    void OnTriggerExit()
    {
        show = false;
    }

    void OnGUI()
    {

        if (show == true)
        {
            GUI.Label(new Rect(100, 450, 1000, 20), "Some people Deserve The Live this world but some people", largeFont);
            GUI.Label(new Rect(200, 500, 1000, 20), "don't deserve the live.", largeFont);
        }
    }
}
