﻿using UnityEngine;
using System.Collections;

public class da : MonoBehaviour {
    public bool show;
    GUIStyle smallFont;
    GUIStyle largeFont;

    void Start()
    {

        largeFont = new GUIStyle();


        largeFont.fontSize = 32;
    }
    void OnTriggerStay()
    {
        show = true;
    }
    void OnTriggerExit()
    {
        show = false;
    }

    void OnGUI()
    {

        if (show == true)
        {
            GUI.Label(new Rect(300, 500, 1000, 20), "Açlık ve fakirlik Dünya'ya hakim.", largeFont);
        }
    }
}
