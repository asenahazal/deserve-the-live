﻿using UnityEngine;
using System.Collections;

public class RUNNewBehaviourScript : MonoBehaviour {

    GUIStyle font;
    public bool show;
    // Use this for initialization
    void Start()
    {
        font = new GUIStyle();
        font.fontSize = 40;
    }
    void OnTriggerStay()
    {
        show = true;
    }
    void OnTriggerExit()
    {
        show = false;
    }
    // Update is called once per frame
    void OnGUI()
    {
        if (show == true)
        {
            GUI.color = Color.white;
            GUI.Label(new Rect(300, 100, 500, 20), "You can not kill a ShadowMan.  So RUN!!", font);
        }

    }
}
