﻿using UnityEngine;
using System.Collections;

public class J : MonoBehaviour {

    public bool show;
    GUIStyle smallFont;
    GUIStyle largeFont;

    void Start()
    {

        largeFont = new GUIStyle();


        largeFont.fontSize = 32;
    }
    void OnTriggerStay()
    {
        show = true;
    }
    void OnTriggerExit()
    {
        show = false;
    }

    void OnGUI()
    {

        if (show == true)
        {
            GUI.Label(new Rect(100, 500, 1000, 20), "Bunu yaptığımız için üzgün değiliz,üzgün olması gereken sizsiniz.", largeFont);
        }
    }
}
