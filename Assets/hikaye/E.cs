﻿using UnityEngine;
using System.Collections;

public class E : MonoBehaviour {

    public bool show;
    GUIStyle smallFont;
    GUIStyle largeFont;

    void Start()
    {

        largeFont = new GUIStyle();


        largeFont.fontSize = 32;
    }
    void OnTriggerStay()
    {
        show = true;
    }
    void OnTriggerExit()
    {
        show = false;
    }

    void OnGUI()
    {

        if (show == true)
        {
            GUI.Label(new Rect(100, 450, 1000, 20), "Bazı insanlar bu Dünyada yaşamayı hakediyor", largeFont);
            GUI.Label(new Rect(200, 500, 1000, 20), "fakat bazıları haketmiyor..", largeFont);
        }
    }
}
