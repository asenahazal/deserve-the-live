﻿using UnityEngine;
using System.Collections;

public class F : MonoBehaviour {
    public bool show;
    GUIStyle smallFont;
    GUIStyle largeFont;

    void Start()
    {

        largeFont = new GUIStyle();


        largeFont.fontSize = 32;
    }
    void OnTriggerStay()
    {
        show = true;
    }
    void OnTriggerExit()
    {
        show = false;
    }

    void OnGUI()
    {

        if (show == true)
        {
            GUI.Label(new Rect(100, 500, 1000, 20), "5 Kıtanın merkezlerinden fırlatılmak üzere 5 tane gaz bombası yaptık.", largeFont);
        }
    }
}
