﻿using UnityEngine;
using System.Collections;

public class H : MonoBehaviour {



    public bool show;
    GUIStyle smallFont;
    GUIStyle largeFont;

    void Start()
    {

        largeFont = new GUIStyle();


        largeFont.fontSize = 32;
    }
    void OnTriggerStay()
    {
        show = true;
    }
    void OnTriggerExit()
    {
        show = false;
    }

    void OnGUI()
    {

        if (show == true)
        {
            GUI.Label(new Rect(200, 500, 1000, 20), "You can live in the new world when you find the exit.", largeFont);
        }
    }
}
